## Carehome Focus Group

### 1.	[Family and Friends] How do you currently know when Margaret is free to be visited i.e. do you call her family?

* We don't
* Sometimes we use WhatsApp when nanny is ill.
* Problem - when nan is ill there are too many visitors
* They tie in with Aimee to limit friends visits
* They use phone and whatsapp
* Informally tell people Margaret is ill - instruct friends to not visit

#### Usual routine:
* Just turn up and visit.
* A specific friend visits every Thursday morning, but sometimes they don't visit due to commitments and then no one visits on a Thursday.
  
#### Lunch outside the carehome:
* Sometimes there is 'overbooking', nan goes out for lunch twice with two different visitors.

#### Illness:
* Margaret has strong medication which can even cause halluscinations.
* When ill and taking the medication, there can be too little downtime.
* In one instance, friend Helen turned up, and then Lillian visited, but didn't know Helen had already visited that day

* Note: there appears to be some reluctance on Margaret's part to admit there can be excessive visitations. Possibly afraid to discourage visitations.

* Patricia says that only family should visit when Margaret is ill.
* But that illness is [a sliding scale]... there are different levels of illness.

* Margaret now says that when she is very ill she doesn't want to see anyone.
* Some visitors are 'gullible' or easily swayed, will be coerced by Nanny to bring her out even when she should not be leaving the carehome.
* Everyone who visits appears to be very close to Nanny already.

#### ALex & Ross (Q1):
* Ross: no set visitation times, just shows up when he's free - it's not structured
* Ross: it's never a problem to just show up - often someone is already there
* Doesn't want structure, spontaneous [visits] are better.


### 2.	[General] Do you think that if Margaret is visited too often or at times not convenient, in your view, for her (for example, twice a day in times close together) it is a negative thing? If so, why do you think so?
### 3.	[General] What sort of things do you do with Margaret when you visit her?
### 4.	 [Carers] How do you currently know if a resident is being visited by friends and family? Do you find that demands from visiting friends and family stretch Millview’s resources such as carer’s running errands due to family/friends’ requests?
### 5.	[Resident] Margaret, do you find that visits are unplanned, and you receive guests who you didn’t expect? If so, are there times where you are tired because you have been visited too many times?
### 6.	[Family] Is there value in knowing when Margaret is being visited by her family and friends? If so what is the value to you?

#### Questions directed to Margaret: How important are the visits? How would you like to influence the visits
* Happy so long as she is visited every day.

#### Are there times when there are too many visits?
* No [emphatic no]

* Margaret only wants family to visit when she is ill
* Margaret is a 'people person', but she doesn't socialize with the other residents of the carehome. Only with family, friends and staff.
* [Note: the term 'people person' was used, but it seems clear that 'extrovert' is what is implied]
* Rarely goes to the dining room
* Prefers to eat alone
* Margaret lived alone for 26 years until she moved to the care home. Shs is not accustomed to making new friends, or socializing outside of a tight knit group of existing friends and family

#### Fatigue
* Margaret feels delighted but tired after a 2 hour visit
* Margaret says it's natural to feel tired after a 2 hour visit
* Gordon: At family dinners with 6 attendees, Margaret gets tired towards the end of the dinner
* Particia: Nanny reports feeling 'perfect' to the social worker, but 5 minutes later she will be fatigued and asleep.

#### Millview: Carehome visitation patterns (general)
* Consistency in visitation frequency is the most important factor.
* Frequent visiting followed by a dramatic drop in frequency is painful for the resident.
* Example: a daughter visited her father every day for the first 2 weeks of residency and then stopped visiting altogehter. This caused great distress for the resident.
* At XMas 3/70 residents stayed with their families [I thought this was 3/17, but the majority of the group thought it was 3/70]
  
* Aimee/Debbie know if residents should be visited, but they cannot overtly encoourage or discourage visits
* The carehome is first and foremost a 'home' with autonomy preserved whenever practically possible
* Residents should not be visited when there is a viral outbreak wihtin the carehome. They give notices but they are not allowed to prevent visits.
* Visitors during a flu outbreak is a major issue for the carehome
* Visiting for 4 hours is excessive and counterproductive

#### Time of day
* Nanny doesn't like night time
* She watches TV (snooker, tennis are her favourites)

### 7.	[Resident] Margaret, what value does being visited by family and friends bring to you? Could you give an example of this?


### 8.	[Carers] Do you think it would be beneficial to have visibility of each residents’ visits from family and friends? If so, can you explain what the benefits would be for you? But also, the negatives if there are any.  


### 9.	[General] We are looking to develop a shared calendar application, which works similar to a WhatsApp group in which participants are added to a single calendar for a resident in the care home setting. It allows coordination of friends and family and creates visibility across all the resident’s network of when friends/family are visiting. Entries into the calendar have to be approved by a minimum of one calendar admin, such as there is on WhatsApp groups. Can you share your views on this initial proposition and if you believe it would add value or not add value for you personally? Can you explain your reasons?

#### Can you think of a time when coordination happened very well/not well?
* Better to know if Margaret is available, especially for those who are far away.

#### Was it ever difficult?
* Not really
* Millview: this family is unique
* Millview: The family are large and very supportive
* Millview: If there's an appointment or Margaret is gone - we can see that the person who travelled a long way for the visit is disappointed.
* Millview: The district nurse showed up unanounced and nobody knew why. But Margaret was gone at that time [I'm not 100% sure of this - did they say Margaret was gone, or that the district nurse was occupying Margarets time when a visitor came].
* Millview (Aimee): we never know when visitors will come
* Catherine: went to the district nurse to get medications because I was taking Margaret out

#### Ross' proposal:
* Communication of information between family/nurse/carehome is better than a scheduler

#### Medication management:
* Mixing of drugs [drug doses] could be an issue
* No one gives medications except Millview becuase they don't know what has already been given to Margaret
* Someone [I thtink it was Patricia] gave an example of when Margaret was suffering greatly from itchiness, but the visitor was afraid to give Margaret the best medication because the visitor was not 100% sure that Margaret had not already received the medication. The visitor was forced to only provide less effective ointment on cautionary grounds.
* On sleep overs, the family are given the appropriate medications and administration schedule.
* Millview: maybe an SMS sent by the family member who is with Margaret, to indicate the medications that were administered while outside of the carehome.... [the Millview representative said 'an SMS to indicate activity', but it seemed to be in relation to medication]

#### Ross - bed chart
* Ross: finds the bed chart is the best source of information about Nanny's care.
* Eg. why was she awake at 2:45am last night.

#### Ross - other families feel guilt
* Ross: guesses that other families feel guilty and would appreciate knowing data on Nanny's care
* For example, an online bed chart
* Patricia and friend were very vocal about having visibility of the bed chart
* Millview: families having access to information about Margaret would reduce phone calls.
* Millview: the system could be used to report incidents (eg. a fall)

#### Question to carehome... how do other families behave
* The dependent rarely gets daily visits
* Average for an active family: 4 visits per week
* Average for an inactive family: 0.5 visits per week [1 visit every two weeks]
  
#### Discussions around an 'informatin sharing' tool
* Calendar is good, but as a means to share info and not a scheduler
* Catherine: forgot to share an eye-doctor appointment with the rest of the family
* Could encourage [other kinds of] families to visit
* Showing visiblitly of residents timetables and historic activity could highlight levels of lonliness. For example, families could see that the resident didn't attend a scheduled event and is isolated.
* Carehome could take pictures of the residents and share via an app
* Ross: suggests a 'Care Home Industry' targetted app that families use as well. A selling point for care homes that use the app.
* Carehome driven service, not a family driven service.






