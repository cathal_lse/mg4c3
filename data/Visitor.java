public class Visitor {
    private long id;
    private long uuid;
    private long uuid_family;
    private long uuid_user;
    private long trust_level;
    private long fCreatedTs;
    private long fModifiedTs;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the uuid_family
     */
    public long getUuid_family() {
        return uuid_family;
    }

    /**
     * @param uuid_family the uuid_family to set
     */
    public void setUuid_family(long uuid_family) {
        this.uuid_family = uuid_family;
    }

    /**
     * @return the uuid_user
     */
    public long getUuid_user() {
        return uuid_user;
    }

    /**
     * @param uuid_user the uuid_user to set
     */
    public void setUuid_user(long uuid_user) {
        this.uuid_user = uuid_user;
    }

    /**
     * @return the trust_level
     */
    public long getTrust_level() {
        return trust_level;
    }

    /**
     * @param trust_level the trust_level to set
     */
    public void setTrust_level(long trust_level) {
        this.trust_level = trust_level;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}