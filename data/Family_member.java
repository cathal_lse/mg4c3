public class Family_member {
    private long id;
    private long uuid;
    private long uuid_family;
    private long uuid_user;
    private long uuid_permission_type;
    private long uuid_family_member;
    private long fCreatedTs;
    private long fModifiedTs;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the uuid_family
     */
    public long getUuid_family() {
        return uuid_family;
    }

    /**
     * @param uuid_family the uuid_family to set
     */
    public void setUuid_family(long uuid_family) {
        this.uuid_family = uuid_family;
    }

    /**
     * @return the uuid_user
     */
    public long getUuid_user() {
        return uuid_user;
    }

    /**
     * @param uuid_user the uuid_user to set
     */
    public void setUuid_user(long uuid_user) {
        this.uuid_user = uuid_user;
    }

    /**
     * @return the uuid_permission_type
     */
    public long getUuid_permission_type() {
        return uuid_permission_type;
    }

    /**
     * @param uuid_permission_type the uuid_permission_type to set
     */
    public void setUuid_permission_type(long uuid_permission_type) {
        this.uuid_permission_type = uuid_permission_type;
    }

    /**
     * @return the uuid_family_member
     */
    public long getUuid_family_member() {
        return uuid_family_member;
    }

    /**
     * @param uuid_family_member the uuid_family_member to set
     */
    public void setUuid_family_member(long uuid_family_member) {
        this.uuid_family_member = uuid_family_member;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}