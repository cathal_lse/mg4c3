public class Family {
    private long id;
    private long uuid;
    private long fName;
    private long administrative;
    private long fCreatedTs;
    private long fModifiedTs;
    private long uuid_user_primary_user;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the fName
     */
    public long getfName() {
        return fName;
    }

    /**
     * @param fName the fName to set
     */
    public void setfName(long fName) {
        this.fName = fName;
    }

    /**
     * @return the administrative
     */
    public long getAdministrative() {
        return administrative;
    }

    /**
     * @param administrative the administrative to set
     */
    public void setAdministrative(long administrative) {
        this.administrative = administrative;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    /**
     * @return the uuid_user_primary_user
     */
    public long getUuid_user_primary_user() {
        return uuid_user_primary_user;
    }

    /**
     * @param uuid_user_primary_user the uuid_user_primary_user to set
     */
    public void setUuid_user_primary_user(long uuid_user_primary_user) {
        this.uuid_user_primary_user = uuid_user_primary_user;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}