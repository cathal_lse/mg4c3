public class Event_list {
    private long id;
    private long uuid;
    private long uuid_care_home;
    private long uuid_event_type;
    private long fName;
    private long fDescription;
    private long fCreatedTs;
    private long fModifiedTs;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the uuid_care_home
     */
    public long getUuid_care_home() {
        return uuid_care_home;
    }

    /**
     * @param uuid_care_home the uuid_care_home to set
     */
    public void setUuid_care_home(long uuid_care_home) {
        this.uuid_care_home = uuid_care_home;
    }

    /**
     * @return the uuid_event_type
     */
    public long getUuid_event_type() {
        return uuid_event_type;
    }

    /**
     * @param uuid_event_type the uuid_event_type to set
     */
    public void setUuid_event_type(long uuid_event_type) {
        this.uuid_event_type = uuid_event_type;
    }

    /**
     * @return the fName
     */
    public long getfName() {
        return fName;
    }

    /**
     * @param fName the fName to set
     */
    public void setfName(long fName) {
        this.fName = fName;
    }

    /**
     * @return the fDescription
     */
    public long getfDescription() {
        return fDescription;
    }

    /**
     * @param fDescription the fDescription to set
     */
    public void setfDescription(long fDescription) {
        this.fDescription = fDescription;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}