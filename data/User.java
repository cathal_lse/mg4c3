public class User {
    private long id;
    private long uuid;
    private long uuid_user_type;
    private long fNameFirst;
    private long fNameLast;
    private long fAddress;
    private long fPhone;
    private long fEmail;
    private long email_username;
    private long password;
    private long bio_metric_A;
    private long fCreatedTs;
    private long fModifiedTs;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the uuid_user_type
     */
    public long getUuid_user_type() {
        return uuid_user_type;
    }

    /**
     * @param uuid_user_type the uuid_user_type to set
     */
    public void setUuid_user_type(long uuid_user_type) {
        this.uuid_user_type = uuid_user_type;
    }

    /**
     * @return the fNameFirst
     */
    public long getfNameFirst() {
        return fNameFirst;
    }

    /**
     * @param fNameFirst the fNameFirst to set
     */
    public void setfNameFirst(long fNameFirst) {
        this.fNameFirst = fNameFirst;
    }

    /**
     * @return the fNameLast
     */
    public long getfNameLast() {
        return fNameLast;
    }

    /**
     * @param fNameLast the fNameLast to set
     */
    public void setfNameLast(long fNameLast) {
        this.fNameLast = fNameLast;
    }

    /**
     * @return the fAddress
     */
    public long getfAddress() {
        return fAddress;
    }

    /**
     * @param fAddress the fAddress to set
     */
    public void setfAddress(long fAddress) {
        this.fAddress = fAddress;
    }

    /**
     * @return the fPhone
     */
    public long getfPhone() {
        return fPhone;
    }

    /**
     * @param fPhone the fPhone to set
     */
    public void setfPhone(long fPhone) {
        this.fPhone = fPhone;
    }

    /**
     * @return the fEmail
     */
    public long getfEmail() {
        return fEmail;
    }

    /**
     * @param fEmail the fEmail to set
     */
    public void setfEmail(long fEmail) {
        this.fEmail = fEmail;
    }

    /**
     * @return the email_username
     */
    public long getEmail_username() {
        return email_username;
    }

    /**
     * @param email_username the email_username to set
     */
    public void setEmail_username(long email_username) {
        this.email_username = email_username;
    }

    /**
     * @return the password
     */
    public long getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(long password) {
        this.password = password;
    }

    /**
     * @return the bio_metric_A
     */
    public long getBio_metric_A() {
        return bio_metric_A;
    }

    /**
     * @param bio_metric_A the bio_metric_A to set
     */
    public void setBio_metric_A(long bio_metric_A) {
        this.bio_metric_A = bio_metric_A;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}