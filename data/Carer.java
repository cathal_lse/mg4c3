public class Carer {
    private long id;
    private long uuid;
    private long uuid_care_home;
    private long uuid_user;
    private long fName;
    private long fPhone;
    private long fCreatedTs;
    private long fModifiedTs;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the uuid_care_home
     */
    public long getUuid_care_home() {
        return uuid_care_home;
    }

    /**
     * @param uuid_care_home the uuid_care_home to set
     */
    public void setUuid_care_home(long uuid_care_home) {
        this.uuid_care_home = uuid_care_home;
    }

    /**
     * @return the uuid_user
     */
    public long getUuid_user() {
        return uuid_user;
    }

    /**
     * @param uuid_user the uuid_user to set
     */
    public void setUuid_user(long uuid_user) {
        this.uuid_user = uuid_user;
    }

    /**
     * @return the fName
     */
    public long getfName() {
        return fName;
    }

    /**
     * @param fName the fName to set
     */
    public void setfName(long fName) {
        this.fName = fName;
    }

    /**
     * @return the fPhone
     */
    public long getfPhone() {
        return fPhone;
    }

    /**
     * @param fPhone the fPhone to set
     */
    public void setfPhone(long fPhone) {
        this.fPhone = fPhone;
    }

    /**
     * @return the fCreatedTs
     */
    public long getfCreatedTs() {
        return fCreatedTs;
    }

    /**
     * @param fCreatedTs the fCreatedTs to set
     */
    public void setfCreatedTs(long fCreatedTs) {
        this.fCreatedTs = fCreatedTs;
    }

    /**
     * @return the fModifiedTs
     */
    public long getfModifiedTs() {
        return fModifiedTs;
    }

    /**
     * @param fModifiedTs the fModifiedTs to set
     */
    public void setfModifiedTs(long fModifiedTs) {
        this.fModifiedTs = fModifiedTs;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }



}